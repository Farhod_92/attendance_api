package com.tax.attendance_api.controller;

import com.tax.attendance_api.payload.ApiResponse;
import com.tax.attendance_api.payload.AttendanceDTO;
import com.tax.attendance_api.service.AttendanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/attendance")
public class AttendanceController {
    @Autowired
    private AttendanceService attendanceService;

    @PostMapping("/comeOrGo")
    public ResponseEntity<?> studentComeOrGo(@RequestBody AttendanceDTO attendanceDTO){
        ApiResponse apiResponse = attendanceService.studentComeOrGo(attendanceDTO);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }
}
