package com.tax.attendance_api.controller;

import com.tax.attendance_api.service.AttendanceService;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RabbitmqTestController {

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private AttendanceService attendanceService;

    @PostMapping("/emit")
    public ResponseEntity<String> emit(@RequestBody String message){
         //amqpTemplate.convertAndSend("myQueue", message);
        attendanceService.sendAttendancesToAnotherApiByRabbitmq();
        return ResponseEntity.ok("Success sent to myQueue");
    }
}
