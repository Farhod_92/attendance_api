package com.tax.attendance_api.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AttendanceDTO{
    @NotNull(message = "student id can't be empty")
    private Integer studentId;

    @NotNull(message = "student must come or go")
    private boolean come;
}
