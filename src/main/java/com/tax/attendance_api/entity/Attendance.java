package com.tax.attendance_api.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "attendance_api")
public class Attendance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private Integer id;

    private Integer studentId;

    private boolean come;

    private boolean sent;

    @CreationTimestamp
    @Column(updatable = false)
    private Timestamp time;
}
