package com.tax.attendance_api.repository;


import com.tax.attendance_api.entity.Attendance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AttendanceRepository extends JpaRepository<Attendance, Integer> {
    List<Attendance> findAllBySentIsFalse();
    boolean deleteByStudentId(Integer studentId);
}
