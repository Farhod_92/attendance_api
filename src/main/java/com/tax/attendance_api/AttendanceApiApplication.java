package com.tax.attendance_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class AttendanceApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(AttendanceApiApplication.class, args);
    }

}
