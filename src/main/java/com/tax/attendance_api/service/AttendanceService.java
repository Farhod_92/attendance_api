package com.tax.attendance_api.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tax.attendance_api.entity.Attendance;
import com.tax.attendance_api.payload.ApiResponse;
import com.tax.attendance_api.payload.AttendanceDTO;
import com.tax.attendance_api.repository.AttendanceRepository;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class AttendanceService {
    @Autowired
    private AttendanceRepository attendanceRepository;

    @Autowired
    private AmqpTemplate amqpTemplate;

    public ApiResponse studentComeOrGo(AttendanceDTO attendanceDTO) {
        Attendance attendance = new Attendance();
        attendance.setStudentId(attendance.getStudentId());
        attendance.setCome(attendanceDTO.isCome());
        attendanceRepository.save(attendance);
        return new ApiResponse("student came or went", true);
    }


    //per two hours
    @Scheduled(cron = "* 2 * * * *")
    private void sendAttendancesToAnotherApi(){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        List<Attendance> allBySentIsFalse = attendanceRepository.findAllBySentIsFalse();
        for (Attendance attendance : allBySentIsFalse) {
            AttendanceDTO attendanceDTO = new AttendanceDTO(attendance.getStudentId(), attendance.isCome());
            HttpEntity<?> entity = new HttpEntity<>(attendanceDTO,headers);
            RestTemplate restTemplate = new RestTemplate();
            String url = "http://localhost:8080/student/comeOrGo";
            ApiResponse response = restTemplate.exchange(url, HttpMethod.POST, entity, ApiResponse.class).getBody();
            if(response.isSuccess()){
                attendance.setSent(true);
                attendanceRepository.save(attendance);
            }
            }
        }

        //per two hours
    @Scheduled(cron = "* 2 * * * *")
    public void sendAttendancesToAnotherApiByRabbitmq(){

//        List<Attendance> allBySentIsFalse = new ArrayList<>(Arrays.asList(
//                new Attendance(1, 1, true, false, Timestamp.from(Instant.now())),
//                new Attendance(2, 3, true, false, Timestamp.from(Instant.now())),
//                new Attendance(3, 4, true, false, Timestamp.from(Instant.now())),
//                new Attendance(4, 5, true, false, Timestamp.from(Instant.now()))
//        ));
        List<Attendance> allBySentIsFalse = attendanceRepository.findAllBySentIsFalse();
        for (Attendance attendance : allBySentIsFalse) {
                AttendanceDTO attendanceDTO = new AttendanceDTO(attendance.getStudentId(), attendance.isCome());
                ObjectMapper objectMapper = new ObjectMapper();
            String s = null;
            try {
                s = objectMapper.writeValueAsString(attendanceDTO);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            amqpTemplate.convertAndSend("attendanceQueue",s);
                attendance.setSent(true);
                attendanceRepository.save(attendance);

            }
        }

    }

